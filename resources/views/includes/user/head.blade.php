	<title>Breif | Blogging is Passion</title>
	<meta charset="utf-8" />

	<meta name="viewport" content="width=device-width, initial-scale=1" />

	<!-- Bootstrap -->
	<link rel='stylesheet' href="{!! asset('public/user/css/bootstrap.min.css') !!}"/>

	<!-- Font Awesome -->
	<link rel='stylesheet' href="{!! asset('public/user/css/font-awesome.min.css') !!}"/>

	<!-- Carousel -->
	<link rel='stylesheet' href="{!! asset('public/user/bundles/custom.css') !!}"/>
	

	<!-- Main Style -->
	<link rel='stylesheet' href="{!! asset('public/user/css/style.css')!!}"/>


	<!-- Google Fonts -->
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:400,800,900' rel='stylesheet' type='text/css'>

