<div id="instagram-footer">
		</div>

		<!--back-to-top-->

		<div id="back-to-top">
			<a href="#top"><i class="fa fa-arrow-up"></i></a>
		</div>

		<footer class="text-center footer">
			<div class="container">
				<div class="row">
					<div class="full">
						<ul class="quick-link">

							<li><a href="#" target="_blank"><i class="fa fa-facebook"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" target="_blank"><i class="fa fa-instagram"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-pinterest"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-google-plus"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-tumblr"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-youtube-play"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-dribbble"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-soundcloud"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-vimeo-square"></i> </a></li>
							<li><a href="#" target="_blank"><i class="fa fa-rss"></i> </a></li>

						</ul>

						<div class="copy-right">
							<p>copyright,&copy; 2017 - All Rights Reserved. <a href="#">Breif</a></p>
						</div>
					</div>
				</div>
			</div>
		</footer>

