<div class="col-md-4">
                <div class="sidebar" id="sidebar">
                    <!-- About -->
                    <section class="blurb">
                        <h2 class="title">ABOUT ME</h2>

                        <a href="single-post.html" class="image"><img class="img-responsive" src="../d33wubrfki0l68.cloudfront.net/6bd1c010c306435cc65f24db8d1b3aea1a594034/e4ead/images/aboutme.jpg" alt="about me" /></a>
                        <div class="author-widget">
                            <h4 class="author-name">Catherine Doe</h4>
                            <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod amet placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at phasellus sed ultricies.</p>
                        </div>
                        <div class="social">
                            <ul class="icons">
                                <li><a href="#" target="_blank"><i class="fa fa-facebook"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-instagram"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-pinterest"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-google-plus"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-tumblr"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-youtube-play"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-dribbble"></i> </a></li>
                                <li><a href="#" target="_blank"><i class="fa fa-soundcloud"></i> </a></li>

                            </ul>
                        </div>
                    </section>

                    <!-- Mini Posts -->
                    <section>
                        <h2 class="title">POPULAR POSTS</h2>
                        <div class="mini-posts">

                            <!-- Mini Post -->
                            <article class="mini-post">
                                <header>
                                    <h3><a href="#">Vitae sed condimentum</a></h3>
                                    <time class="published" datetime="2017-01-14">October 20, 2017</time>
                                    <a href="#" class="author"><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                                </header>
                                <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/9f7b8cddc227ce575e95a5e0e8036d2affa7bd61/4c6fd/images/side-post01.jpg" alt="" /></a>
                            </article>

                            <!-- Mini Post -->
                            <article class="mini-post">
                                <header>
                                    <h3><a href="#">Rutrum neque accumsan</a></h3>
                                    <time class="published" datetime="2017-01-14">October 19, 2017</time>
                                    <a href="#" class="author"><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                                </header>
                                <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/2ddeb695c93f356c216e80357c3c7053c714e8e3/f5240/images/side-post02.jpg" alt="" /></a>
                            </article>

                            <!-- Mini Post -->
                            <article class="mini-post">
                                <header>
                                    <h3><a href="#">Odio congue mattis</a></h3>
                                    <time class="published" datetime="2017-01-14">October 18, 2017</time>
                                    <a href="#" class="author"><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                                </header>
                                <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/e0cd1cb786eea86edcefa4d134a4e8db0bad9f8b/51722/images/side-post03.jpg" alt="" /></a>
                            </article>

                            <!-- Mini Post -->
                            <article class="mini-post">
                                <header>
                                    <h3><a href="#">Vitae sed condimentum</a></h3>
                                    <time class="published" datetime="2017-01-14">October 20, 2017</time>
                                    <a href="#" class="author"><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                                </header>
                                <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/9f7b8cddc227ce575e95a5e0e8036d2affa7bd61/4c6fd/images/side-post01.jpg" alt="" /></a>
                            </article>
                        </div>
                    </section>
                    <!-- Posts List -->
                    <section>
                        <h2 class="title">LATEST POSTS</h2>
                        <ul class="posts">
                            <li>
                                <article>
                                    <header>
                                        <h3><a href="#">adventure cliffs climb climber</a></h3>
                                        <time class="published" datetime="2017-01-14">October 20, 2017</time>
                                    </header>
                                    <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/36ce118d10e6ec7d10650942d388d5c0fd108b08/e0b11/images/small-side-post01.jpg" alt="" /></a>
                                </article>
                            </li>
                            <li>
                                <article>
                                    <header>
                                        <h3><a href="#">Convallis maximus nisl mattis nunc id lorem</a></h3>
                                        <time class="published" datetime="2017-01-14">October 15, 2017</time>
                                    </header>
                                    <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/75de56c7f92e6948888a18d482137ceb4d05abc9/56abd/images/small-side-post02.jpg" alt="" /></a>
                                </article>
                            </li>
                            <li>
                                <article>
                                    <header>
                                        <h3><a href="#">green and white convertible coupe on day time</a></h3>
                                        <time class="published" datetime="2017-01-14">October 10, 2017</time>
                                    </header>
                                    <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/684b1b28d633e9544cb5f26bf92d7f6f7d9e59df/d48b1/images/small-side-post03.jpg" alt="" /></a>
                                </article>
                            </li>
                            <li>
                                <article>
                                    <header>
                                        <h3><a href="#">green and white convertible coupe on day time</a></h3>
                                        <time class="published" datetime="2017-01-14">October 10, 2017</time>
                                    </header>
                                    <a href="#" class="image"><img src="../d33wubrfki0l68.cloudfront.net/eaf0d42e8a129f20c1bdc9a2ba4ba29cd20fedbb/09583/images/small-side-post04.jpg" alt="" /></a>
                                </article>
                            </li>
                        </ul>
                    </section>

                    <section>
                        <div class="widget HTML">
                            <h2 class="title">INSTGRAM</h2>
                            <div class="widget-content">
                                <div class="instagram-feeds row-gallery">
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/42cd81bc6fdf02b168a6c7731d6d41614c096b42/e5a9a/images/instagram01.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/2be5117f0f8791105eaa8f6225940dc85de3d501/05e6b/images/instagram02.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/c4b5d84463de168990caf785b476171e2933bbe7/2ec3b/images/instagram03.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/2a98d70ce3ed39151b234625df6e89fdf723ed34/06daf/images/instagram04.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/434639103b5df129424f68274416a82923e2a1b9/d319f/images/instagram05.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/a33e2f5ca15a1a696a91663f30a66f33867114d4/7a887/images/instagram06.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/e3c5b50e266fd4d6371d64136ca97e0c194a807e/ef3f4/images/instagram07.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/ae08e2764ca18872d1665a7608bbaeacb6e54797/0858e/images/instagram08.jpg" alt="Instagram Image"></a>
                                    <a href="#" class="col-sm-4 col-xs-3"><img src="../d33wubrfki0l68.cloudfront.net/42cd81bc6fdf02b168a6c7731d6d41614c096b42/ee714/images/instagram09.jpg" alt="Instagram Image"></a>
                                </div>
                            </div>
                        </div>
                    </section> <!-- End inta -->

                </div> <!-- End Sidebar -->
            </div><!-- End-col-md-4 -->
