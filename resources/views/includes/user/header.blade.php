<!--<div id="preloader">
    <div id="status">  </div>
</div>-->


<!--Navigation-->
<nav class="navbar navbar-default navbar-static-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main_nav" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href='index.html' class='navbar-brand'><img src="../d33wubrfki0l68.cloudfront.net/10fb8ea3421c6fbf36315e0dad88ec2e1d9ac693/3aee6/images/logo.png" alt="logo"></a>
        </div>
        <div class="collapse navbar-collapse" id="main_nav">
            <div class=" pull-right hidden-xs hidden-sm">
                <ul class="nav social-links">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                </ul>

            </div>
            <ul class="nav navbar-nav navbar-left">
                <li><a href="#" >Home </a>
                    <ul class="dropdown-menu">
                        <li><a href='home-2.html'>Home 2</a></li>
                        <li><a href='home-3.html'>Home 3</a></li>
                    </ul>
                </li>
                <li><a href='category.html'>Fashion</a></li>
                <li><a href='category.html'>People</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages</a>
                    <ul class="dropdown-menu">
                        <li><a href='leftsidebar.html'>left sidebar</a></li>
                        <li><a href='fullwidth.html'>full width</a></li>
                        <li><a href='404.html'>error 404</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Post Format</a>
                    <ul class="dropdown-menu">
                        <li><a href='image-post.html'>Image Post</a></li>
                        <li><a href='audio-post.html'>Audio Post</a></li>
                        <li><a href='video-post.html'>Video Post</a></li>
                        <li><a href='full-post.html'>full Post</a></li>
                    </ul>
                </li>
                <li><a href='contact.html'>contact</a></li>

            </ul>
        </div>
    </div>
</nav>

<!-- End navigation -->

