@extends('layouts.user')


@section('content')
                <article class="post">
                    <header>
                        <div class="title">
                            <h2><a href="single-post.html">women camping hiking travel</a></h2>
                            <p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
                        </div>
                        <div class="meta">
                            <time class="published" datetime="2017-01-14">November 1, 2017</time>
                            <a href="#" class="author"><span class="name">CATHERINE DOE</span><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                        </div>
                    </header>
                    <a href="single-post.html" class="image featured"><img src="../d33wubrfki0l68.cloudfront.net/4bf284e26c018f9425f060084cc646b882907e4e/3352b/images/large-post01.jpg" alt="" /></a>
                    <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla.</p>
                    <footer>
                        <ul class="actions">
                            <li><a href="single-post.html" class="button big">Continue Reading</a></li>
                        </ul>
                        <ul class="stats">
                            <li><a href="#">General</a></li>
                            <li><a href="#" class="icon fa-heart">28</a></li>
                            <li><a href="#" class="icon fa-comment">128</a></li>
                        </ul>
                    </footer>
                </article>

                <!-- Post -->
                <article class="post">
                    <header>
                        <div class="title">
                            <h2><a href="single-post.html">2 girls hugging each other outdoor during daytime</a></h2>
                            <p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
                        </div>
                        <div class="meta">
                            <time class="published" datetime="2017-01-14">October 25, 2017</time>
                            <a href="#" class="author"><span class="name">CATHERINE DOE</span><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                        </div>
                    </header>
                    <a href="single-post.html" class="image featured"><img src="../d33wubrfki0l68.cloudfront.net/263ef793f330f075186494191143906f24533639/e97d5/images/large-post02.jpg" alt="" /></a>
                    <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper.</p>
                    <footer>
                        <ul class="actions">
                            <li><a href="single-post.html" class="button big">Continue Reading</a></li>
                        </ul>
                        <ul class="stats">
                            <li><a href="#">General</a></li>
                            <li><a href="#" class="icon fa-heart">28</a></li>
                            <li><a href="#" class="icon fa-comment">128</a></li>
                        </ul>
                    </footer>
                </article>

                <!-- Post -->
                <article class="post">
                    <header>
                        <div class="title">
                            <h2><a href="single-post.html">food salad Healthy lunch</a></h2>
                            <p>Lorem ipsum dolor amet nullam consequat etiam feugiat</p>
                        </div>
                        <div class="meta">
                            <time class="published" datetime="2017-01-14">October 22, 2017</time>
                            <a href="#" class="author"><span class="name">CATHERINE DOE</span><img src="../d33wubrfki0l68.cloudfront.net/0ec005ed0443e9a8d1c16f2bccbc9c76cb66e0f5/aaa2d/images/author-avatar.png" alt="" /></a>
                        </div>
                    </header>
                    <a href="single-post.html" class="image featured"><img src="../d33wubrfki0l68.cloudfront.net/6e75c3fb673d0bb8be79c701b4929c1fc15c24eb/21746/images/large-post03.jpg" alt="" /></a>
                    <p>Mauris neque quam, fermentum ut nisl vitae, convallis maximus nisl. Sed mattis nunc id lorem euismod placerat. Vivamus porttitor magna enim, ac accumsan tortor cursus at. Phasellus sed ultricies mi non congue ullam corper. Praesent tincidunt sed tellus ut rutrum. Sed vitae justo condimentum, porta lectus vitae, ultricies congue gravida diam non fringilla. Cras vehicula tellus eu ligula viverra, ac fringilla turpis suscipit. Quisque vestibulum rhoncus ligula.</p>
                    <footer>
                        <ul class="actions">
                            <li><a href="single-post.html" class="button big">Continue Reading</a></li>
                        </ul>
                        <ul class="stats">
                            <li><a href="#">General</a></li>
                            <li><a href="#" class="icon fa-heart">28</a></li>
                            <li><a href="#" class="icon fa-comment">128</a></li>
                        </ul>
                    </footer>
                </article>
                <!-- Pagination -->
                <ul class="actions pagination">
                    <li><a href="#" class="disabled button big previous">Previous Page</a></li>
                    <li><a href="#" class="button big next">Next Page</a></li>
                </ul>
            

 
@stop





