<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
        @include('includes.user.head')
    </head>
    <body >
        @include('includes/user/header')
        @include('includes/user/slider')
        <div id="main">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
   @yield('content')
                    </div>
                   @include('includes.user.sidebar')
                </div>
            </div>
            @include('includes.user.footer')
        </div>
        @include('includes.user.foot')
    </body>
</html>
