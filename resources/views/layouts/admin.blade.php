<!DOCTYPE html>
<html lang="en">
<head>
    @include('includes.admin.head')
</head>
<body>
<div id="wrapper">
    @include('includes.admin.nav')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Dashboard</h1>
            </div>
        </div>
    @yield('content')
    </div>
</div>
@include('includes.admin.foot')
</body>
</html>
